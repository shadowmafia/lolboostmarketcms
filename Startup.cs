using DataAccessLayerAbstraction;
using DataAccessLayerImplementation;
using LoLBoostMarketCMS.Classes;
using LoLBoostMarketCMS.Classes.Services.PasswordHashing;
using LoLBoostMarketCMS.Settings.ControllersSettings;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace LoLBoostMarketCMS
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // services.AddMvc(options => options.Filters.Add(typeof(AuthorizationFilter)));
            services.AddElasticLoLBoostMarketDal(dalSettingsContainFile: "dataAccessLayerSettings.json");
            services.InitAuthorization(Configuration);
            services.InitLocalization(Configuration);
            services.InitEmailSender(Configuration);
            services.AddDistributedMemoryCache();
            services.AddMemoryCache();

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromSeconds(10);
                options.Cookie.IsEssential = true;
            });

            IConfigurationSection userProfileControllerSettingsSection = Configuration.GetSection("UserProfileControllerSettings");
            services.Configure<UserProfileControllerSettings>(userProfileControllerSettingsSection);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IWebHostEnvironment env,
            IDataAccessLayer dal)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseRequestLocalization();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSession();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            var serviceProvider = app.ApplicationServices;
            var passwordHasher = serviceProvider.GetService<PasswordHasher>();

            dal.InitSuperAdminIfNotExist(Configuration, passwordHasher);
        }
    }
}