﻿using LoLBoostMarketCMS.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace LoLBoostMarketCMS.Classes.Services
{
    public class EmailSender
    {
        private readonly IOptions<EmailSenderSettings> _emailSenderSettings;

        public EmailSender(
            IOptions<EmailSenderSettings> emailSenderSettings
        )
        {
            _emailSenderSettings = emailSenderSettings ?? throw new ArgumentNullException(nameof(emailSenderSettings));
        }

        public async Task SendSystemEmailAsync(string email, string subject, string messageBody, List<string> AttachmentsFilesPaths = null)
        {
            MailMessage message = new MailMessage
            {
                IsBodyHtml = true,
                From = new MailAddress(_emailSenderSettings.Value.DisplayEmail,
                    _emailSenderSettings.Value.DisplayName) //отправитель сообщения
            };

            message.To.Add(email);
            message.Subject = subject;
            message.Body = messageBody;

            if (AttachmentsFilesPaths != null)
            {
                foreach (var filePath in AttachmentsFilesPaths)
                {
                    message.Attachments.Add(new Attachment(filePath));
                }
            }

            await Task.Run(() =>
            {
                using SmtpClient client = new SmtpClient("smtp.gmail.com")
                {
                    Credentials = new NetworkCredential(_emailSenderSettings.Value.Login, _emailSenderSettings.Value.Password),
                    Port = 587,
                    EnableSsl = true
                };

                client.Send(message);
            });
        }
    }
}