﻿namespace LoLBoostMarketCMS.Classes.Services.PasswordHashing
{
    public sealed class HashingOptions
    {
        public int Iterations { get; set; } = 10000;
    }
}
