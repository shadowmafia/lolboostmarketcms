﻿using LoLBoostMarketCMS.Settings;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace LoLBoostMarketCMS.Classes.Services.GoogleAuth
{
    public class GoogleAuthHelper
    {
        const string AuthorizationEndpoint = "https://accounts.google.com/o/oauth2/v2/auth";
        const string TokenRequestUri = "https://www.googleapis.com/oauth2/v4/token";
        const string UserInfoRequestUri = "https://www.googleapis.com/oauth2/v3/userinfo";
        const string CodeChallengeMethod = "S256";
        private readonly IOptions<AuthorizationSettings> _authorizationSettings;

        public GoogleAuthHelper(
            IOptions<AuthorizationSettings> authorizationSettings
        )
        {
            _authorizationSettings = authorizationSettings ?? throw new ArgumentNullException(nameof(authorizationSettings));
        }

        public string CreateRedirectToGoogleAuth(string googleResponceRedirect, ISession session)
        {
            string state = RandomDataBase64Url(32);
            string codeVerifier = RandomDataBase64Url(32);
            string codeChallenge = Base64UrlEncodeNoPadding(Sha256(codeVerifier));
            string redirectUrl = googleResponceRedirect;

            session.SetString("state", state);
            session.SetString("codeVerifier", codeVerifier);
            session.SetString("codeChallenge", codeChallenge);

            return $"{AuthorizationEndpoint}?response_type=code&scope=openid%20profile%20email&redirect_uri={System.Uri.EscapeDataString(redirectUrl)}&client_id={_authorizationSettings.Value.GoogleSettings.ClientId}&state={state}&code_challenge={codeChallenge}&code_challenge_method={CodeChallengeMethod}";
        }

        public async Task<GoogleUserModel> GetGoogleAuthUserModel(string code, string codeVerifier, string redirectUri)
        {
            string tokenRequestBody = $"code={code}&redirect_uri={System.Uri.EscapeDataString(redirectUri)}&client_id={_authorizationSettings.Value.GoogleSettings.ClientId}&code_verifier={codeVerifier}&client_secret={_authorizationSettings.Value.GoogleSettings.ClientSecret}&scope=&grant_type=authorization_code";

            HttpWebRequest tokenRequest = (HttpWebRequest)WebRequest.Create(TokenRequestUri);
            tokenRequest.Method = "POST";
            tokenRequest.ContentType = "application/x-www-form-urlencoded";
            tokenRequest.Accept = "Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            byte[] byteVersion = Encoding.ASCII.GetBytes(tokenRequestBody);
            tokenRequest.ContentLength = byteVersion.Length;
            Stream stream = tokenRequest.GetRequestStream();
            await stream.WriteAsync(byteVersion, 0, byteVersion.Length);
            stream.Close();

            try
            {
                WebResponse tokenResponse = await tokenRequest.GetResponseAsync();

                using (StreamReader reader = new StreamReader(tokenResponse.GetResponseStream()))
                {
                    string responseText = await reader.ReadToEndAsync();
                    Dictionary<string, string> tokenEndpointDecoded = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseText);

                    string accessToken = tokenEndpointDecoded["access_token"];

                    return await GetUserInfoCall(accessToken);
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = ex.Response as HttpWebResponse;

                    if (response != null)
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            string responseText = await reader.ReadToEndAsync();
                        }
                    }
                }
            }

            return null;
        }

        #region Private methods

        private async Task<GoogleUserModel> GetUserInfoCall(string accessToken)
        {
            HttpWebRequest userInfoRequest = (HttpWebRequest)WebRequest.Create(UserInfoRequestUri);
            userInfoRequest.Method = "GET";
            userInfoRequest.Headers.Add(string.Format("Authorization: Bearer {0}", accessToken));
            userInfoRequest.ContentType = "application/x-www-form-urlencoded";
            userInfoRequest.Accept = "Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";

            WebResponse userInfoResponse = await userInfoRequest.GetResponseAsync();

            using (StreamReader userInfoResponseReader = new StreamReader(userInfoResponse.GetResponseStream()))
            {
                string userInfoResponseText = await userInfoResponseReader.ReadToEndAsync();

                return JsonConvert.DeserializeObject<GoogleUserModel>(userInfoResponseText);
            }
        }

        private static string Base64UrlEncodeNoPadding(byte[] buffer)
        {
            string base64 = Convert.ToBase64String(buffer);

            // Converts base64 to base64url.
            base64 = base64.Replace("+", "-");
            base64 = base64.Replace("/", "_");
            // Strips padding.
            base64 = base64.Replace("=", "");

            return base64;
        }

        private static string RandomDataBase64Url(uint length)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] bytes = new byte[length];
            rng.GetBytes(bytes);

            return Base64UrlEncodeNoPadding(bytes);
        }

        private static byte[] Sha256(string inputStirng)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(inputStirng);
            SHA256Managed sha256 = new SHA256Managed();

            return sha256.ComputeHash(bytes);
        }

        #endregion
    }
}