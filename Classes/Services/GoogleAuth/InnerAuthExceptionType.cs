﻿namespace LoLBoostMarketCMS.Classes.Services.GoogleAuth
{
    public enum InnerAuthExceptionType
    {
        UnknownProblem,
        UnknownGoogleProblem,
        GoogleEmailNotVerified,
        UserWithThisGoogleAccountNotFound,
        EmailNotConfirmed
    }
}
