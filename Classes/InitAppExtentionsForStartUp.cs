﻿using DataAccessLayerAbstraction;
using DataAccessLayerAbstraction.MarketData.Mapping;
using DataAccessLayerAbstraction.MarketData.Mapping.CmsUserClasses;
using DataAccessLayerImplementation;
using LoLBoostMarketCMS.Classes.Services;
using LoLBoostMarketCMS.Classes.Services.GoogleAuth;
using LoLBoostMarketCMS.Classes.Services.PasswordHashing;
using LoLBoostMarketCMS.Settings;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Globalization;

namespace LoLBoostMarketCMS.Classes
{
    public static class InitAppExtensionsForStartUp
    {
        public static void InitAuthorization(this IServiceCollection services, IConfiguration configuration)
        {
            IConfigurationSection authorizationSettingsSection = configuration.GetSection("AuthorizationSettings");

            services.Configure<AuthorizationSettings>(authorizationSettingsSection);
            services.AddSingleton<GoogleAuthHelper>();
            services.AddSingleton<PasswordHasher>();

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = new Microsoft.AspNetCore.Http.PathString("/Authorization");
                    options.AccessDeniedPath = new Microsoft.AspNetCore.Http.PathString("/Authorization");
                });
        }

        public static void InitLocalization(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddLocalization(options => options.ResourcesPath = "Resources");
            services.AddControllersWithViews()
                .AddViewLocalization();

            LocalizationSettings languageSettings = new LocalizationSettings();
            IConfigurationSection languageSettingsSection = configuration.GetSection("LocalizationSettings");
            languageSettingsSection.Bind(languageSettings);

            services.Configure<LocalizationSettings>(languageSettingsSection);

            var supportedCultures = new List<CultureInfo>();

            foreach (var item in languageSettings.AllSupportedLanguages)
            {
                supportedCultures.Add(new CultureInfo(item.Culture));
            }

            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.DefaultRequestCulture = new RequestCulture(languageSettings.DefaultLanguageCulture);
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });
        }

        public static void InitEmailSender(this IServiceCollection services, IConfiguration configuration)
        {
            IConfigurationSection emailSettingsSection = configuration.GetSection("EmailSenderSettings");

            services.Configure<EmailSenderSettings>(emailSettingsSection);
            services.AddSingleton<EmailSender>();
        }

        public static void InitSuperAdminIfNotExist(this IDataAccessLayer dal, IConfiguration configuration, PasswordHasher passwordHasher)
        {
            ((ElasticsearchEntityManagerBase<CmsUser>)dal.MarketData.CmsUser.Manager).DeleteIndexAsync().GetAwaiter().GetResult();

            CmsUser admin = new CmsUser();
            configuration.GetSection("DefaultSuperAdminData").Bind(admin);
            admin.CmsUserType = CmsUserType.SuperAdmin;
            admin.Password = passwordHasher.HashPassword(admin.Password);

            if (dal.MarketData.CmsUser.Searcher.GetCmsUser(admin.Id).GetAwaiter().GetResult() == null)
            {
                dal.MarketData.CmsUser.Manager.AddOrUpdate(admin).GetAwaiter().GetResult();
            }
        }
    }
}
