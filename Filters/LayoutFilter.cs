﻿using DataAccessLayerAbstraction;
using LoLBoostMarketCMS.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace LoLBoostMarketCMS.Filters
{
    public class LayoutFilter : IActionFilter
    {
        IDataAccessLayer _dal;
        public LayoutFilter(IDataAccessLayer dal)
        {
            _dal = dal ?? throw new ArgumentNullException(nameof(dal));
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            throw new NotImplementedException();
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var controller = (HomeController)context.Controller;
        }
    }
}
