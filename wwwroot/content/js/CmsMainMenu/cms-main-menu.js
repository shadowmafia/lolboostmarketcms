﻿$(document).ready(function () {
    $(document).on('click', '#sign-out-btn', function (e) {
        window.location.href = $(this).attr("data-url-action");
    });
    //
    $(document).on('click', '.main-menu-item ', function (e) {
        window.location.href = $(this).attr("data-url-action");
    });
});