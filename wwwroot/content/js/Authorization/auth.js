﻿let requestInProgress = false;
let errAfterRequest = false;
let isValidate = true;

$(document).ready(function () {
    $(document).on('click', '#signIn', function(e) {
        TryAuthorize(e);
    });

    $(document).on('click', '#googleIn', function (e) {
        window.location.href = $('#hidden-data').attr('google-in-url');
    });

    $(document).on('focus', '#login, #password', function (e) {
        if (errAfterRequest) {
            $("#i-g-1").removeClass('input-err');
            $("#i-g-2").removeClass('input-err');
            errAfterRequest = false;
        }
    });

    $(document).on('focus', '#login', function (e) {
        $("#i-g-1").removeClass('input-err');
    });

    $(document).on('focus', ' #password', function (e) {
        $("#i-g-2").removeClass('input-err');
    });

    $(document).on('blur', '#login', function (e) {
        new Validator().ValidateLogin();
    });

    $(document).on('blur', '#password', function (e) {
        new Validator().ValidatePassword();
    });

    $(document).on('keypress', '#login', function (event) {
        if (event.keyCode === 13) {
            if (new Validator().ValidateLogin() === true) {
                $('#password').focus();
            }
        }
    });

    $(document).on('keypress', '#password', function(event) {
        if (event.keyCode === 13) {
            if (new Validator().ValidatePassword() === true) {
                $("#signIn").focus();
                TryAuthorize(event);
            }
        }
    });
});

function TryAuthorize(e) {
    const validator = new Validator();
    const hiddenData = $('#hidden-data');

    if (requestInProgress === false) {
        if (validator.Valid() === true) {
            requestInProgress = true;
        } else {
            return;
        }
    } else {
        return;
    }

    $(".ajs-error").remove();
    $('#owelay').css('display', 'flex');
    alertify.message(hiddenData.attr('please-wait-text'), 3);

   
    const data = {
        id: $('#login').val(),
        password: $('#password').val(),
        isRemember: $('#rememberUser').is(":checked")
    };

    $.ajax({
        type: 'POST',
        url: hiddenData.attr('sign-in-url'),
        data: data,
        error: function (error) {
            $("#owelay").css('display', 'none');
            $(".ajs-message").remove();

            alertify.error(error.responseText, 3);

            $("#i-g-1").addClass('input-err');
            $("#i-g-2").addClass('input-err');

            requestInProgress = false;
            errAfterRequest = true;
        },
        success: function (data, textStatus, request) {
            alertify.success(hiddenData.attr('authorized-text'), 3);

            setTimeout(function() {
                window.location.href = data;
            }, 1000);
        }
    });
}

 function Validator() {
    this.isValid = true;
    this.login = $('#login').val();
    this.password = $('#password').val();

    this.ValidateLogin = function() {
        if (this.login.length <= 2) {
            this.isValid = false;
            $("#i-g-1").addClass('input-err');

            return false;
        } else {
            $("#i-g-1").removeClass('input-err');

            return true;
        }
    };

    this.ValidatePassword = function() {
        if (this.password.length <= 2) {
            this.isValid = false;
            $("#i-g-2").addClass('input-err');

            return false;
        } else {
            $("#i-g-2").removeClass('input-err');

            return true;
        }
    };

     this.Valid = function () {
         const hiddenData = $('#hidden-data');

         if (this.ValidateLogin() === false) {
             alertify.error(hiddenData.attr('correct-login-text'), 3);
        }

        if (this.ValidatePassword() === false) {
            alertify.error(hiddenData.attr('correct-password-text'), 3);
        }

        return this.isValid;
    };
}