﻿let MinUserNameLength = null;
let MinPasswordLength = null;
let EmailValidationRegex = null;
let timer = null;
let timeOutValue = null;
let croppier = null;

const allowedImgExtensions = /\.(jpe?g|png|gif|bmp)$/i;

$(document).ready(function () {
    InitSettings();
    IntUserEditorEvents();
    InitUserEditorPopUpEvents();

    InitEmailTooltip();
    TryInitGmailTooltip();
    InitSwtTooltip();
    InitSwt2Tooltip();

    InitTimeOutTimer();
});

function InitSettings() {
    let helpData = $('#user-editor-help-data');
    MinUserNameLength = helpData.attr('data-min-user-name-length');
    MinPasswordLength = helpData.attr('data-min-password-length');
    EmailValidationRegex = new RegExp(helpData.attr('data-email-validation-regex'));
}

function IntUserEditorEvents() {

    $(document).on('click', '.copy-to-clip-board', function (e) {
        if (CopyToClipboard($('#cms-user-id').text())) {
            alertify.success($('#user-editor-help-data').attr('data-userid-was-copied-text'));
        }
    });

    $(document).on('click', '.edit-user-name-btn', function (e) {
        const fancyData = $('.change-user-name-promt');

        $.fancybox.open(
            fancyData,
            {
                touch: false,
                toolbar: false,
                smallBtn: true,
                beforeShow: function () {

                },
                afterClose: function () {

                },
                afterShow: function () {

                }
            }
        );
    });

    $(document).on('click', '.edit-default-email-btn', function (e) {
        if ($(this).hasClass('disable-icon')) {
            return;
        }

        const fancyData = $('.change-email-promt');

        $.fancybox.open(
            fancyData,
            {
                touch: false,
                toolbar: false,
                smallBtn: true,
                beforeShow: function () {

                },
                afterClose: function () {

                },
                afterShow: function () {

                }
            }
        );
    });

    $(document).on('click', '#confirm-email-btn', function (e) {
        if ($(this).hasClass('inactive-btn')) {
            return;
        }

        TryConfirmEmail();
    });

    $(document).on('click', '#connect-google-btn', function (e) {
        window.location.href = $(this).attr('data-href');
    });

    $(document).on('click', '#disconnect-google-btn', function (e) {
        ShowGlobalPreloader();
        let href = $(this).attr('data-href');

        $.ajax({
            type: 'POST',
            url: href,
            data: null,
            error: function (error) {
                HideGlobalPreloader();
            },
            success: function (data, textStatus, request) {
                alertify.success($('#user-editor-help-data').attr('data-google-was-disconnected-text'), 3);
                HideGlobalPreloader();

                ReplaceUserEditor(data);
            }
        });
    });



    $(document).on('click', '.cmsue-profile-img-container', function (e) {
        const url = $(this).attr('data-href');
        ShowGlobalPreloader();

        $.ajax({
            type: 'POST',
            url: url,
            data: null,
            error: function (error) {
                HideGlobalPreloader();
            },
            success: function (data, textStatus, request) {
                HideGlobalPreloader();

                $.fancybox.open(
                    data,
                    {
                        touch: false,
                        toolbar: false,
                        smallBtn: true,
                        beforeShow: function () {

                        },
                        afterClose: function () {

                        },
                        afterShow: function () {
                            InitCroppier();
                        }
                    }
                );
            }
        });
    });

    $(document).on('click', '#change-password-btn', function (e) {
        const fancyData = $('.change-password-promt');

        $.fancybox.open(
            fancyData,
            {
                touch: false,
                toolbar: false,
                smallBtn: true,
                beforeShow: function () {

                },
                afterClose: function () {

                },
                afterShow: function () {

                }
            }
        );
    });

    $(document).on('click', '#select-new-image-btn', function (e) {
        $('#image-input').click();
    });

    $(document).on('change', '#image-input', function (e) {
        try {
            UploadFileToCroppie(this.files[0]);
        }
        catch (e) {
            UploadFileToCroppie(null);
        }
    });

    $(document).on('click', '#save-new-image-btn', function (e) {
        TrySaveImage();
    });
}

function InitUserEditorPopUpEvents() {
    $(document).on('click', '.confirm-name-change', function (e) {
        TryChangeName(this);
    });

    $(document).on('click', '.confirm-email-change', function (e) {
        if ($(this).hasClass('inactive-btn')) {
            return;
        }

        TryChangeEmail();
    });

    $(document).on('click', '.confirm-password-change', function (e) {
        TryChangePassword(this);
    });

    $(document).on('click', '#promt-cancel, .custom-promt-close-btn', function (e) {
        $.fancybox.close();
    });

    $(document).on('keypress', '#new-name', function (event) {
        if (event.keyCode === 13) {
            TryChangeName(this);
        }
    });

    $(document).on('keypress', '#new-password', function (event) {
        if (event.keyCode === 13) {
            if (ValidatePassword() === true) {
                $(this).parent().removeClass('input-err');
                $('#confirm-password').focus();
            } else {
                alertify.error(`${$('#user-editor-help-data').attr('data-password-must-be-more-then-text')} (${(MinPasswordLength - 1)}) ${$('#user-editor-help-data').attr('data-character-text')}.`);
            }
        }
    });

    $(document).on('keypress', '#confirm-password', function (event) {
        if (event.keyCode === 13) {
            TryChangePassword(this);
        }
    });

    $(document).on('keypress', '#new-email', function (event) {
        if (event.keyCode === 13) {
            TryChangeEmail(this);
        }
    });

    $(document).on('focus', '#new-name, #new-email, #new-password, #confirm-password', function (e) {
        $(this).parent().removeClass('input-err');
    });

    $(document).on('blur', '#new-name', function (e) {
        ValidateUserName();
    });

    $(document).on('blur', '#new-email', function (e) {
        ValidateEmail();
    });

    $(document).on('blur', '#new-password', function (e) {
        ValidatePassword();
    });

    $(document).on('blur', '#confirm-password', function (e) {
        ValidateConfirmPassword();
    });
}

function InitTimeOutTimer() {
    if ($("#email-timeot-value")) {
        clearInterval(timer);

        timeOutValue = $("#email-timeot-value").attr('data-timeout-value');

        timer = setInterval(TimerTick, 1000);
    }
}

function TimerTick() {
    timeOutValue = timeOutValue - 1;

    if (timeOutValue > 0) {
        $(".email-timeot-timer").text(`(${timeOutValue})`);
    }
    else {
        $(".email-timeot-timer").remove();
        $("#email-timeot-value").remove();
        $(".disable-icon").removeClass('disable-icon');
        $('#confirm-email-btn').removeClass('inactive-btn');
    }
}


function ValidateUserName() {
    const userNameInput = $(".fancybox-is-open").find('#new-name');
    const isValid = userNameInput.val().length >= MinUserNameLength;

    if (isValid === false) {
        userNameInput.parent().addClass('input-err');
    }

    return isValid;
}

function ValidateEmail() {
    const emailInput = $(".fancybox-is-open").find('#new-email');
    const isValid = EmailValidationRegex.test(emailInput.val());

    if (isValid === false) {
        emailInput.parent().addClass('input-err');
    }

    return isValid;
}

function ValidatePassword() {
    const passwordInput = $(".fancybox-is-open").find('#new-password');
    const isValid = passwordInput.val() && passwordInput.val().length >= MinPasswordLength;

    if (isValid === false) {
        passwordInput.parent().addClass('input-err');
    }

    return isValid;
}

function ValidateConfirmPassword() {
    const passwordInput = $(".fancybox-is-open").find('#new-password');
    const confirmPasswordInput = $(".fancybox-is-open").find('#confirm-password');

    const isValid = passwordInput.val() === confirmPasswordInput.val();

    if (isValid === false) {
        confirmPasswordInput.parent().addClass('input-err');
    }

    return isValid;
}


function TryInitGmailTooltip() {
    const gmailStatusIcon = document.querySelector('#gmail-is-not-confirmed');
    const gmailTooltip = document.querySelector('#gmail-tooltip');

    let popperInstance = null;

    function create() {
        popperInstance = Popper.createPopper(gmailStatusIcon, gmailTooltip, {
            placement: 'right',
            modifiers: [
                {
                    name: 'offset',
                    options: {
                        offset: [0, 8]
                    }
                }
            ]
        });
    }

    function destroy() {
        if (popperInstance) {
            popperInstance.destroy();
            popperInstance = null;
        }
    }

    function show() {
        gmailTooltip.setAttribute('data-show', '');
        create();
    }

    function hide() {
        gmailTooltip.removeAttribute('data-show');
        destroy();
    }

    if (gmailStatusIcon) {
        const showEvents = ['mouseenter', 'focus'];
        const hideEvents = ['mouseleave', 'blur'];

        showEvents.forEach(event => {
            gmailStatusIcon.addEventListener(event, show);
        });

        hideEvents.forEach(event => {
            gmailStatusIcon.addEventListener(event, hide);
        });
    }
}

function InitEmailTooltip() {
    const emailStatusIcon = document.querySelector('#email-confirmed-status');
    const emailTooltip = document.querySelector('#email-tooltip');

    let popperInstance = null;

    function create() {
        popperInstance = Popper.createPopper(emailStatusIcon, emailTooltip, {
            placement: 'right',
            modifiers: [
                {
                    name: 'offset',
                    options: {
                        offset: [0, 8]
                    }
                }
            ]
        });
    }

    function destroy() {
        if (popperInstance) {
            popperInstance.destroy();
            popperInstance = null;
        }
    }

    function show() {
        emailTooltip.setAttribute('data-show', '');
        create();
    }

    function hide() {
        emailTooltip.removeAttribute('data-show');
        destroy();
    }

    const showEvents = ['mouseenter', 'focus'];
    const hideEvents = ['mouseleave', 'blur'];

    showEvents.forEach(event => {
        emailStatusIcon.addEventListener(event, show);
    });

    hideEvents.forEach(event => {
        emailStatusIcon.addEventListener(event, hide);
    });
}

function InitSwtTooltip() {
    const emailStatusIcon = document.querySelector('#ews-icon');
    const emailTooltip = document.querySelector('#ews-tooltip');

    if (document.querySelector('#ews-icon') === null) {
        return;
    }

    let popperInstance = null;

    function create() {
        popperInstance = Popper.createPopper(emailStatusIcon, emailTooltip, {
            placement: 'right',
            modifiers: [
                {
                    name: 'offset',
                    options: {
                        offset: [0, 8]
                    }
                }
            ]
        });
    }

    function destroy() {
        if (popperInstance) {
            popperInstance.destroy();
            popperInstance = null;
        }
    }

    function show() {
        emailTooltip.setAttribute('data-show', '');
        create();
    }

    function hide() {
        emailTooltip.removeAttribute('data-show');
        destroy();
    }

    const showEvents = ['mouseenter', 'focus'];
    const hideEvents = ['mouseleave', 'blur'];

    showEvents.forEach(event => {
        emailStatusIcon.addEventListener(event, show);
    });

    hideEvents.forEach(event => {
        emailStatusIcon.addEventListener(event, hide);
    });
}

function InitSwt2Tooltip() {
    const emailStatusIcon = document.querySelector('#edit-default-email');
    const emailTooltip = document.querySelector('#edit-default-email-tooltip');

    if (document.querySelector('#email-timeot-value') === null) {
        return;
    }

    let popperInstance = null;

    function create() {
        popperInstance = Popper.createPopper(emailStatusIcon, emailTooltip, {
            placement: 'right',
            modifiers: [
                {
                    name: 'offset',
                    options: {
                        offset: [0, 8]
                    }
                }
            ]
        });
    }

    function destroy() {
        if (popperInstance) {
            popperInstance.destroy();
            popperInstance = null;
        }
    }

    function show() {
        if ($(emailStatusIcon).hasClass('disable-icon') === false) {
            return;
        }

        emailTooltip.setAttribute('data-show', '');
        create();
    }

    function hide() {
        emailTooltip.removeAttribute('data-show');
        destroy();
    }

    const showEvents = ['mouseenter', 'focus'];
    const hideEvents = ['mouseleave', 'blur'];

    showEvents.forEach(event => {
        emailStatusIcon.addEventListener(event, show);
    });

    hideEvents.forEach(event => {
        emailStatusIcon.addEventListener(event, hide);
    });
}


function TryChangeName() {
    if (ValidateUserName() === false) {
        alertify.error(`${$('#user-editor-help-data').attr('data-user-name-must-be-more-then-text')} (${(MinUserNameLength - 1)}) ${$('#user-editor-help-data').attr('data-character-text')}.`);

        return;
    }

    ShowGlobalPreloader();
    let href = $('.confirm-name-change').attr('data-href');
    const userName = $(".fancybox-is-open").find('#new-name');

    $.ajax({
        type: 'POST',
        url: href,
        data: {
            username: userName.val()
        },
        error: function (error) {
            HideGlobalPreloader();
            alertify.error(error.responseText, 3);
        },
        success: function (data, textStatus, request) {
            alertify.success($('#user-editor-help-data').attr('data-user-name-was-changed-text'), 3);
            HideGlobalPreloader();

            ReplaceUserEditor(data);

            $.fancybox.close();
        }
    });
}

function TryChangePassword() {
    if (ValidatePassword() === false) {
        alertify.error(`${$('#user-editor-help-data').attr('data-password-must-be-more-then-text')} (${(MinPasswordLength - 1)}) ${$('#user-editor-help-data').attr('data-character-text')}.`);

        return false;
    }

    if (ValidateConfirmPassword() === false) {
        alertify.error($('#user-editor-help-data').attr('data-passwords-do-not-match-text'));

        return false;
    }

    ShowGlobalPreloader();
    let href = $('.confirm-password-change').attr('data-href');
    const password = $(".fancybox-is-open").find('#new-password').val();

    $.ajax({
        type: 'POST',
        url: href,
        data: {
            password: password
        },
        error: function (error) {
            HideGlobalPreloader();
            alertify.error(error, 3);
        },
        success: function (data, textStatus, request) {
            alertify.success($('#user-editor-help-data').attr('data-password-was-changed-text'), 3);
            HideGlobalPreloader();
            $.fancybox.close();
        }
    });
}

function TryChangeEmail() {
    if (ValidateEmail() === false) {
        alertify.error($('#user-editor-help-data').attr('data-invalid-email-text'));

        return;
    }

    ShowGlobalPreloader();
    let href = $('.confirm-email-change').attr('data-href');
    const email = $(".fancybox-is-open").find('#new-email').val();

    $.ajax({
        type: 'POST',
        url: href,
        data: {
            email: email
        },
        error: function (error) {
            HideGlobalPreloader();
            alertify.error(error.responseText, 3);
        },
        success: function (data, textStatus, request) {
            alertify.success($('#user-editor-help-data').attr('data-email-was-changed-text'), 3);
            $.fancybox.close();
            ReplaceUserEditor(data);
            HideGlobalPreloader();
            TryConfirmEmail();
        }
    });
}

function TryConfirmEmail() {
    const item = $('#confirm-email-btn');

    ShowGlobalPreloader();
    alertify.warning($('#user-editor-help-data').attr('data-please-waut-text'), 3);
    let href = item.attr('data-href');

    $.ajax({
        type: 'POST',
        url: href,
        data: null,
        error: function (error) {
            alertify.error(error.responseText, 3);
            HideGlobalPreloader();
        },
        success: function (data, textStatus, request) {
            alertify.success($('#user-editor-help-data').attr('data-email-was-send-text'), 3);
            ReplaceUserEditor(data);
            HideGlobalPreloader();
        }
    });
}

function TrySaveImage() {
    const fileInput = document.getElementById('image-input');

    if (fileInput.files && fileInput.files[0]) {
        if (allowedImgExtensions.test(fileInput.files[0].name) === false) {
            return;
        } else {
            ShowGlobalPreloader();
            const href = $('#save-new-image-btn').attr('data-href');

            croppier.croppie('result', 'blob')
                .then(function (resp) {

                var formData = new FormData();
                formData.append("file", resp);

                $.ajax({
                    type: 'POST',
                    url: href,
                    data: formData,
                    processData: false,
                    contentType: false,
                    error: function (error) {
                        alertify.error(error.responseText, 3);
                        HideGlobalPreloader();
                    },
                    success: function (data, textStatus, request) {
                        window.URL.revokeObjectURL($('#user-avatar').attr('href')); 
                        $.fancybox.close();
                        alertify.success($('#user-editor-help-data').attr('data-profile-picture-has-been-changed-text'), 3);
                        ReplaceUserEditor(data);
                        HideGlobalPreloader();
                    }
                });
            });
        }
    }
}

function ReplaceUserEditor(newEditor) {
    $('#cms-user-profile-editor').replaceWith(newEditor);
    InitEmailTooltip();
    TryInitGmailTooltip();
    InitSwtTooltip();
    InitSwt2Tooltip();
    InitTimeOutTimer();
    InitSettings();
}

function UploadFileToCroppie(file) {
    if (file === null) {
        $('#image-editor-container').css('display', 'none');
        $('#change-image-description').css('display', 'flex');

        return;
    }

    if (allowedImgExtensions.test(file.name) === false) {
        $('#image-editor-container').css('display', 'none');
        $('#change-image-description').css('display', 'flex');
        alertify.error($('#user-editor-help-data').attr('data-invalid-file-format-text'));

        return;
    }

    $('#change-image-description').css('display', 'none');
    $('#image-editor-container').css('display', 'block');

    var reader = new FileReader();

    reader.onload = function (e) {
        croppier.croppie('bind',
        {
            url: e.target.result
        });
    };

    reader.readAsDataURL(file);
    $('#save-new-image-btn').focus();
}

function InitCroppier() {
    croppier = $('#image-editor-container');

    croppier.croppie({
        viewport: {
            width: 150,
            height: 150,
            type: 'circle'
        },
        boundary: {
            width: 700,
            height: 300
        },
        enableExif: true
    });
}