﻿$(document).ready(function() {
    $(document).on('click', '#save-language-btn', function (e) {
        const url = $(this).attr('data-href');
        const data = {
            culture: $('.language-item.selected').attr('data-culture'),
            returnUrl: $('#service-data').attr('data-current-page-url')
        };

        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            error: function (error) {

            },
            success: function (data, textStatus, request) {
                if (request.status === 200) {
                    window.location.href = data;
                }
            }
        });
    });

    $(document).on('click', '.language-item', function (e) {
        $('.language-item').removeClass('selected');
        $(this).addClass('selected');
    });
});