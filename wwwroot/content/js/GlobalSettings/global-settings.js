﻿$(document).ready(function() {
    $(document).on('click', '#global-settings-btn', function (e) {
        ShowSettingsModal();
    });

    $(document).on('click', '#stw-close-btn', function (e) {
        $.fancybox.close();
    });
});

function ShowSettingsModal() {

    $("#global-loading-overlay").addClass('active');

    const modalUrl = $('#global-settings-btn').attr('data-href');

    if (modalUrl.length <= 0) {
        return;
    }

    $.ajax({
        type: 'POST',
        url: modalUrl,
        error: function (error) {

        },
        success: function (data, textStatus, request) {
            $.fancybox.open(
                data,
                {
                    touch: false,
                    toolbar: false,
                    smallBtn: true,
                    beforeShow: function () {
                        $("#global-loading-overlay").removeClass('active');
                    },
                    afterClose: function () {
                       
                    },
                    afterShow: function () {
                       
                    }
                }
            );
        }
    });
}