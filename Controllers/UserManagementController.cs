﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LoLBoostMarketCMS.Controllers
{
    [Authorize]
    public class UserManagerController : Controller
    {
        public IActionResult Index()
        {
            ViewData["bodyBackgroundImageUrl"] = "~/img/UserManager/user-manager-background.jpg";

            return View();
        }
    }
}