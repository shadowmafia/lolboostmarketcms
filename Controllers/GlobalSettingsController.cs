﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace LoLBoostMarketCMS.Controllers
{
    [Authorize]
    public class GlobalSettingsController : Controller
    {
        [AllowAnonymous]
        [HttpPost]
        public IActionResult GetSettingsWindowModal()
        {
            return PartialView("_SettingsWindowModal");
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult ChangeLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return Ok(returnUrl);
        }
    }
}