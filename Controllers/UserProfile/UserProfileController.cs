﻿using DataAccessLayerAbstraction;
using DataAccessLayerAbstraction.MarketData.Mapping;
using DataAccessLayerAbstraction.MarketData.Mapping.CmsUserClasses;
using LoLBoostMarketCMS.Classes.Services;
using LoLBoostMarketCMS.Classes.Services.GoogleAuth;
using LoLBoostMarketCMS.Classes.Services.PasswordHashing;
using LoLBoostMarketCMS.Models.UserProfile;
using LoLBoostMarketCMS.Settings.ControllersSettings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace LoLBoostMarketCMS.Controllers.UserProfile
{
    [Authorize]
    public partial class UserProfileController : Controller
    {
        private readonly IWebHostEnvironment _env;
        private readonly IDataAccessLayer _dal;
        private readonly IMemoryCache _cache;
        private readonly IStringLocalizer<UserProfileController> _localizer;
        private readonly IOptions<UserProfileControllerSettings> _userProfileControllerSettings;
        private readonly GoogleAuthHelper _googleAuthHelper;
        private readonly PasswordHasher _passwordHasher;
        private readonly EmailSender _emailSender;

        public UserProfileController(
            IWebHostEnvironment env,
            IDataAccessLayer dal,
            IMemoryCache cache,
            IStringLocalizer<UserProfileController> localizer,
            IOptions<UserProfileControllerSettings> userProfileControllerSettings,
            GoogleAuthHelper googleAuthHelper,
            PasswordHasher passwordHasher,
            EmailSender emailSender)
        {
            _env = env ?? throw new ArgumentNullException(nameof(env));
            _dal = dal ?? throw new ArgumentNullException(nameof(dal));
            _cache = cache ?? throw new ArgumentNullException(nameof(cache));
            _localizer = localizer ?? throw new ArgumentNullException(nameof(localizer));
            _userProfileControllerSettings = userProfileControllerSettings ?? throw new ArgumentNullException(nameof(userProfileControllerSettings));
            _googleAuthHelper = googleAuthHelper ?? throw new ArgumentNullException(nameof(googleAuthHelper));
            _passwordHasher = passwordHasher ?? throw new ArgumentNullException(nameof(passwordHasher));
            _emailSender = emailSender ?? throw new ArgumentNullException(nameof(emailSender));
        }

        public async Task<IActionResult> Index()
        {
            string userId = ((ClaimsIdentity)User.Identity).Name;

            if (userId == null)
            {
                return BadRequest("User not found");
            }

            ViewData["bodyBackgroundImageUrl"] = "~/img/UserProfile/user-profile-background.jpg";

            CmsUser cmsUser = await _dal.MarketData.CmsUser.Searcher.GetCmsUser(userId);

            BoosterProfile boosterProfile = null;

            if (cmsUser.CmsUserType == CmsUserType.Booster)
            {
                boosterProfile = await _dal.MarketData.BoosterProfile.Searcher.GetBoosterProfile(cmsUser.Id);
            }

            return View(new CmsUserProfileModel
            {
                CmsUser = cmsUser,
                BoosterProfile = boosterProfile
            });
        }
    }
}