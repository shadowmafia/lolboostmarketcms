﻿using DataAccessLayerAbstraction.MarketData.Mapping;
using DataAccessLayerAbstraction.MarketData.Mapping.CmsUserClasses;
using DataAccessLayerImplementation.MarketData.DataManagers;
using LoLBoostMarketCMS.Classes.Helpers;
using LoLBoostMarketCMS.Classes.Services.GoogleAuth;
using LoLBoostMarketCMS.Models.UserProfile;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Drawing;
using System.IO;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace LoLBoostMarketCMS.Controllers.UserProfile
{
    public partial class UserProfileController
    {
        private const string CmsUserProfileImageUploadFolder = "CmsUserUploadedFiles";
        private const string VerifyEmailTokenCacheKey = "verifyEmailTokenCacheKey";
        private const string VerifyEmailTimeOutCacheKey = "verifyEmailTimeOutCacheKey";

        [HttpPost]
        public async Task<IActionResult> GetCmsUserEditor()
        {
            string userId = ((ClaimsIdentity)User.Identity).Name;

            if (userId == null)
            {
                return BadRequest(_localizer["UserNotFound"].Value);
            }

            CmsUser cmsUser = await _dal.MarketData.CmsUser.Searcher.GetCmsUser(userId);

            return PartialView("EditCmsUserProfile", cmsUser);
        }

        [HttpPost]
        public async Task<IActionResult> ChangeUserName(string username)
        {
            string userId = ((ClaimsIdentity)User.Identity).Name;

            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentNullException(nameof(username));
            }

            if (username.Length < _userProfileControllerSettings.Value.EditCmsUserSettings.MinUsernameLength)
            {
                throw new ArgumentOutOfRangeException(nameof(username), $"cant be less then {_userProfileControllerSettings.Value.EditCmsUserSettings.MinUsernameLength} symbols");
            }

            CmsUser currentUser = await _dal.MarketData.CmsUser.Searcher.GetCmsUser(userId);

            if (currentUser.Name == username)
            {
                return BadRequest(_localizer["YouCannotChangeUsernameToTheSame"].Value);
            }

            if ((await _dal.MarketData.CmsUser.Searcher.FindCmsUsers(usernameContain: username)).Count > 0)
            {
                return BadRequest(_localizer["UserWithThisUserNameAlreadyExist"].Value);
            }

            currentUser.Name = username;
            await _dal.MarketData.CmsUser.Manager.AddOrUpdate(currentUser);

            return PartialView("EditCmsUserProfile", currentUser);
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(string password)
        {
            string userId = ((ClaimsIdentity)User.Identity).Name;

            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException(nameof(password));
            }

            if (password.Length < _userProfileControllerSettings.Value.EditCmsUserSettings.MinPasswordLength)
            {
                throw new ArgumentOutOfRangeException(nameof(password), $"cant be less then {_userProfileControllerSettings.Value.EditCmsUserSettings.MinPasswordLength} symbols");
            }

            CmsUser currentUser = await _dal.MarketData.CmsUser.Searcher.GetCmsUser(userId);
            currentUser.Password = _passwordHasher.HashPassword(password);
            await _dal.MarketData.CmsUser.Manager.AddOrUpdate(currentUser);

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> ChangeProfileImage(IFormFile file)
        {
            string userId = ((ClaimsIdentity)User.Identity).Name;

            if (file == null)
            {
                throw new ArgumentNullException(nameof(file));
            }

            string imageFormat = null;

            await using (var ms = new MemoryStream())
            {
                file.CopyTo(ms);
                var fileBytes = ms.ToArray();

                if (IsImage(fileBytes, out string iFormat) == false)
                {
                    throw new ArgumentException($"{nameof(file)} have incorrect file format! (dont image)");
                }
                else
                {
                    imageFormat = iFormat;
                }
            }

            string hashedUserId;

            using (MD5 md5Hash = MD5.Create())
            {
                hashedUserId = GetMd5Hash(md5Hash, userId);
            }

            string filePath = $"/{CmsUserProfileImageUploadFolder}/cmsUserAvatar-{hashedUserId}.{imageFormat.ToLower()}";
            string fullPath = $"{this._env.WebRootPath}{filePath}";

            using (var fileStream = new FileStream(fullPath, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }

            CmsUser currentUser = await _dal.MarketData.CmsUser.Searcher.GetCmsUser(userId);
            currentUser.ProfileImageUrl = filePath;

            await _dal.MarketData.CmsUser.Manager.AddOrUpdate(currentUser);

            return PartialView("EditCmsUserProfile", currentUser);
        }


        #region Image changing

        [HttpPost]
        public IActionResult GetChangeUserImageModal()
        {
            return PartialView("ChangeUserImage");
        }

        #endregion

        #region Email changing

        [HttpPost]
        public async Task<IActionResult> ChangeDefaultEmail(string email)
        {
            string userId = ((ClaimsIdentity)User.Identity).Name;

            if (string.IsNullOrEmpty(email))
            {
                throw new ArgumentNullException(nameof(email));
            }

            string verifyEmailTimeOutKey = $"{VerifyEmailTimeOutCacheKey}_{userId}";
            string verifyEmailTokenKey = $"{VerifyEmailTokenCacheKey}_{userId}";

            if (_cache.TryGetValue(verifyEmailTimeOutKey, out DateTime state))
            {
                var timeOutTime = TimeSpan.FromTicks(state.Ticks - DateTime.UtcNow.Ticks);

                return BadRequest($"{_localizer["TimeoutPleaseWait"].Value} : {timeOutTime.TotalSeconds}");
            }

            Regex regex = new Regex($@"{_userProfileControllerSettings.Value.EditCmsUserSettings.EmailValidationRegex}");
            Match match = regex.Match(email);

            if (match.Success == false)
            {
                throw new ArgumentOutOfRangeException(nameof(email), "Incorrect email format");
            }

            CmsUser currentUser = await _dal.MarketData.CmsUser.Searcher.GetCmsUser(userId);

            if (email.ToUpper() == currentUser.DefaultEmail.Email)
            {
                return BadRequest(_localizer["YouCannotChangeTheEmailAddressToTheSame"].Value);
            }

            if (await _dal.MarketData.CmsUser.Searcher.GetCmsUser(email: email) != null)
            {
                return BadRequest(_localizer["YouCannotChangeTheEmailAddressToTheSame"].Value);
            }

            currentUser.DefaultEmail.Email = email;
            currentUser.DefaultEmail.IsEmailConfirmed = false;
            currentUser.DefaultEmail.LastVerificationEmailDateTime = null;

            _cache.Remove(verifyEmailTokenKey);

            await _dal.MarketData.CmsUser.Manager.AddOrUpdate(currentUser);

            return PartialView("EditCmsUserProfile", currentUser);
        }

        [HttpPost]
        public async Task<IActionResult> ConfirmEmail()
        {
            string userId = ((ClaimsIdentity)User.Identity).Name;
            string verifyEmailTokenKey = $"{VerifyEmailTokenCacheKey}_{userId}";
            string verifyEmailTimeOutKey = $"{VerifyEmailTimeOutCacheKey}_{userId}";

            if (_cache.TryGetValue(verifyEmailTimeOutKey, out DateTime state))
            {
                var timeOutTime = TimeSpan.FromTicks(state.Ticks - DateTime.UtcNow.Ticks);

                return BadRequest($"{_localizer["TimeoutPleaseWait"].Value} : {timeOutTime.TotalSeconds}");
            }

            string randomToken = GenerateRandomHash(_userProfileControllerSettings.Value.EditCmsUserSettings.ConfirmPasswordRandomHashLength);
            await ((CmsUserDataManager)_dal.MarketData.CmsUser.Manager).RefreshIndexAsync();
            CmsUser currentUser = await _dal.MarketData.CmsUser.Searcher.GetCmsUser(userId);

            if (currentUser.DefaultEmail.IsEmailConfirmed)
            {
                return BadRequest(_localizer["EmailAlreadyConfirmed"].Value);
            }

            string viewString = await this.RenderViewAsync<(string, string)>("ConfirmEmailMessage", (userId, randomToken), true);

            try
            {
                await _emailSender.SendSystemEmailAsync(currentUser.DefaultEmail.Email, _localizer["EmailVerification"].Value, viewString);

                _cache.Set(verifyEmailTokenKey, randomToken, DateTimeOffset.UtcNow.AddMinutes(_userProfileControllerSettings.Value.EditCmsUserSettings.VerifyEmailTokenLifeTimeInMinutes));
                _cache.Set(verifyEmailTimeOutKey, DateTime.UtcNow.AddSeconds(_userProfileControllerSettings.Value.EditCmsUserSettings.VerifyEmailTimeOutInSeconds), TimeSpan.FromSeconds(_userProfileControllerSettings.Value.EditCmsUserSettings.VerifyEmailTimeOutInSeconds));
                currentUser.DefaultEmail.LastVerificationEmailDateTime = DateTime.UtcNow;

                await _dal.MarketData.CmsUser.Manager.AddOrUpdate(currentUser);
            }
            catch (Exception e)
            {
                return BadRequest($"{_localizer["EmailSendingError"].Value}: {e}");
            }

            return PartialView("EditCmsUserProfile", currentUser);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> VerifyEmailByToken(string userId, string token)
        {
            string verifyEmailTokenKey = $"{VerifyEmailTokenCacheKey}_{userId}";

            if (_cache.TryGetValue(verifyEmailTokenKey, out string tokenFromCache))
            {
                if (token == tokenFromCache)
                {
                    _cache.Remove(verifyEmailTokenKey);

                    CmsUser currentUser = await _dal.MarketData.CmsUser.Searcher.GetCmsUser(userId);
                    currentUser.DefaultEmail.IsEmailConfirmed = true;
                    currentUser.DefaultEmail.LastVerifiedEmail = currentUser.DefaultEmail.Email;
                    currentUser.DefaultEmail.LastVerificationEmailDateTime = DateTime.UtcNow;

                    await _dal.MarketData.CmsUser.Manager.AddOrUpdate(currentUser);

                    return View("VeiryEmailResultPage", VerifyEmailByTokenState.Verified);
                }
            }

            return View("VeiryEmailResultPage", VerifyEmailByTokenState.TokenNotFount);
        }

        #endregion

        #region Google connection

        [HttpGet]
        public IActionResult ConnectGoogle()
        {
            string redirectUrl = _googleAuthHelper.CreateRedirectToGoogleAuth(
                $"https://{this.HttpContext.Request.Host}{Url.Action("GoogleConnectResponse", "UserProfile")}",
                HttpContext.Session
            );

            return Redirect(redirectUrl);
        }

        [HttpPost]
        public async Task<IActionResult> DisconnectGoogle()
        {
            string userId = ((ClaimsIdentity)User.Identity).Name;
            CmsUser currentUser = await _dal.MarketData.CmsUser.Searcher.GetCmsUser(userId);

            currentUser.GoogleData = null;
            await _dal.MarketData.CmsUser.Manager.AddOrUpdate(currentUser);

            return PartialView("EditCmsUserProfile", currentUser);
        }

        [HttpGet]
        public async Task<IActionResult> GoogleConnectResponse()
        {
            if (string.IsNullOrEmpty(Request.Query["error"]) == false)
            {
                HttpContext.Session.SetString("ProblemType", InnerAuthExceptionType.UnknownGoogleProblem.ToString());

                return RedirectToAction("AuthProblem", "Authorization");
            }

            if (string.IsNullOrEmpty(Request.Query["code"]) || string.IsNullOrEmpty(Request.Query["state"]))
            {
                HttpContext.Session.SetString("ProblemType", InnerAuthExceptionType.UnknownGoogleProblem.ToString());

                return RedirectToAction("AuthProblem", "Authorization");
            }

            if (Request.Query["state"] != HttpContext.Session.GetString("state"))
            {
                HttpContext.Session.SetString("ProblemType", InnerAuthExceptionType.UnknownGoogleProblem.ToString());

                return RedirectToAction("AuthProblem", "Authorization");
            }

            var code = Request.Query["code"];
            string codeVerifier = HttpContext.Session.GetString("codeVerifier");

            GoogleUserModel googleUser = await _googleAuthHelper.GetGoogleAuthUserModel(
                code,
                codeVerifier,
                $"https://{this.HttpContext.Request.Host}{Url.Action("GoogleConnectResponse", "UserProfile")}"
            );

            if (googleUser == null)
            {
                HttpContext.Session.SetString("ProblemType", InnerAuthExceptionType.UnknownGoogleProblem.ToString());

                return RedirectToAction("AuthProblem", "Authorization");
            }

            string userId = ((ClaimsIdentity)User.Identity).Name;
            CmsUser currentUser = await _dal.MarketData.CmsUser.Searcher.GetCmsUser(userId);

            currentUser.GoogleData = new CmsUserGoogleData
            {
                GMail = googleUser.Email,
                GoogleId = googleUser.GoogleId,
                IsGMailVerified = googleUser.EmailVerified,
                IsGoogleConnected = true
            };

            await _dal.MarketData.CmsUser.Manager.AddOrUpdate(currentUser);
            await ((CmsUserDataManager)_dal.MarketData.CmsUser.Manager).RefreshIndexAsync();

            return RedirectToAction("Index", "Home");
        }

        #endregion

        #region private methods

        private static string GenerateRandomHash(int length)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] bytes = new byte[length];
            rng.GetBytes(bytes);

            return Base64UrlEncodeNoPadding(bytes);
        }

        private static string Base64UrlEncodeNoPadding(byte[] buffer)
        {
            string base64 = Convert.ToBase64String(buffer);

            // Converts base64 to base64url.
            base64 = base64.Replace("+", "-");
            base64 = base64.Replace("/", "_");
            // Strips padding.
            base64 = base64.Replace("=", "");

            return base64;
        }

        public bool IsImage(byte[] data, out string format)
        {
            var dataIsImage = false;
            format = null;

            using (var imageReadStream = new MemoryStream(data))
            {
                try
                {
                    using (var possibleImage = Image.FromStream(imageReadStream))
                    {
                        format = possibleImage.RawFormat.ToString();
                    }

                    dataIsImage = true;
                }
                catch
                {
                    dataIsImage = false;
                }
            }

            return dataIsImage;
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        #endregion
    }
}