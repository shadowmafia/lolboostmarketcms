﻿using DataAccessLayerAbstraction.MarketData.Mapping;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;

namespace LoLBoostMarketCMS.Controllers.UserProfile
{
    public partial class UserProfileController
    {
        [HttpPost]
        public async Task<IActionResult> GetBoosterProfileEditor()
        {
            string userId = ((ClaimsIdentity)User.Identity).Name;

            if (userId == null)
            {
                return BadRequest("User not found");
            }

            BoosterProfile boosterProfile = await _dal.MarketData.BoosterProfile.Searcher.GetBoosterProfile(userId);

            return View("EditBoosterProfile", boosterProfile);
        }
    }
}
