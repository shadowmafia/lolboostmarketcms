﻿using DataAccessLayerAbstraction;
using DataAccessLayerAbstraction.MarketData.Mapping;
using LoLBoostMarketCMS.Classes.Services.GoogleAuth;
using LoLBoostMarketCMS.Classes.Services.PasswordHashing;
using LoLBoostMarketCMS.Settings;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace LoLBoostMarketCMS.Controllers
{
    [Authorize]
    public class AuthorizationController : Controller
    {
        private readonly IDataAccessLayer _dal;
        private readonly IStringLocalizer<AuthorizationController> _localizer;
        private readonly GoogleAuthHelper _googleAuthHelper;
        private readonly PasswordHasher _passwordHasher;
        private readonly IOptions<AuthorizationSettings> _authSettings;

        public AuthorizationController(
            IDataAccessLayer dal,
            IStringLocalizer<AuthorizationController> localizer,
            GoogleAuthHelper googleAuthHelper,
            PasswordHasher passwordHasher,
            IOptions<AuthorizationSettings> authSettings
        )
        {
            _dal = dal ?? throw new ArgumentNullException(nameof(dal));
            _localizer = localizer ?? throw new ArgumentNullException(nameof(localizer));
            _googleAuthHelper = googleAuthHelper ?? throw new ArgumentNullException(nameof(googleAuthHelper));
            _passwordHasher = passwordHasher ?? throw new ArgumentNullException(nameof(passwordHasher));
            _authSettings = authSettings ?? throw new ArgumentNullException(nameof(authSettings));
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            var userIdentity = (ClaimsIdentity)User.Identity;

            if (string.IsNullOrEmpty(userIdentity.Name) == false)
            {
                return RedirectToAction("Index", "Home");
            };

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> SignIn(string id, string password, bool isRemember)
        {
            if (id?.Length < 3 || password?.Length < 3)
            {
                throw new ArgumentOutOfRangeException($"{nameof(id)} or {nameof(password)} incorrect lenght");
            }

            CmsUser userForAthorize = await _dal.MarketData.CmsUser.Searcher.GetCmsUser(id, id);

            if (
                userForAthorize == null ||
                _passwordHasher.VerifyHashedPassword(userForAthorize.Password, password) == PasswordVerificationResult.Failed
            )
            {
                return BadRequest(_localizer["UseNotFound"].Value);
            }

            if (userForAthorize.GoogleData.GMail == id?.ToUpper() &&
                userForAthorize.GoogleData.IsGoogleConnected == false)
            {
                return BadRequest(_localizer["UseNotFound"].Value);
            }

            if (userForAthorize.DefaultEmail.Email == id?.ToUpper() && userForAthorize.DefaultEmail.IsEmailConfirmed == false)
            {
                HttpContext.Session.SetString("ProblemType", InnerAuthExceptionType.EmailNotConfirmed.ToString());

                return Ok(Url.Action("AuthProblem", "Authorization"));
            }

            await Authenticate(userForAthorize, isRemember);

            return Ok(Url.Action("Index", "Home"));
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult GoogleSignIn()
        {
            string redirectUrl = _googleAuthHelper.CreateRedirectToGoogleAuth(
                $"https://{this.HttpContext.Request.Host}{Url.Action("GoogleResponse", "Authorization")}",
                HttpContext.Session
            );

            return Redirect(redirectUrl);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> GoogleResponse()
        {
            if (string.IsNullOrEmpty(Request.Query["error"]) == false)
            {
                HttpContext.Session.SetString("ProblemType", InnerAuthExceptionType.UnknownGoogleProblem.ToString());

                return RedirectToAction("AuthProblem");
            }

            if (string.IsNullOrEmpty(Request.Query["code"]) || string.IsNullOrEmpty(Request.Query["state"]))
            {
                HttpContext.Session.SetString("ProblemType", InnerAuthExceptionType.UnknownGoogleProblem.ToString());

                return RedirectToAction("AuthProblem");
            }

            if (Request.Query["state"] != HttpContext.Session.GetString("state"))
            {
                HttpContext.Session.SetString("ProblemType", InnerAuthExceptionType.UnknownGoogleProblem.ToString());

                return RedirectToAction("AuthProblem");
            }

            var code = Request.Query["code"];
            string codeVerifier = HttpContext.Session.GetString("codeVerifier");

            GoogleUserModel googleUser = await _googleAuthHelper.GetGoogleAuthUserModel(
                code,
                codeVerifier,
                $"https://{this.HttpContext.Request.Host}{Url.Action("GoogleResponse", "Authorization")}"
            );

            if (googleUser == null)
            {
                HttpContext.Session.SetString("ProblemType", InnerAuthExceptionType.UnknownGoogleProblem.ToString());

                return RedirectToAction("AuthProblem");
            }

            if (googleUser.EmailVerified == false)
            {
                HttpContext.Session.SetString("ProblemType", InnerAuthExceptionType.GoogleEmailNotVerified.ToString());

                return RedirectToAction("AuthProblem");
            }

            CmsUser cmsUserForAuthorize = await _dal.MarketData.CmsUser.Searcher.GetCmsUserByGoogleId(googleUser.GoogleId);

            if (cmsUserForAuthorize == null)
            {
                HttpContext.Session.SetString("ProblemType", InnerAuthExceptionType.UserWithThisGoogleAccountNotFound.ToString());

                return RedirectToAction("AuthProblem");
            }

            if (cmsUserForAuthorize.GoogleData.IsGoogleConnected == false)
            {
                HttpContext.Session.SetString("ProblemType", InnerAuthExceptionType.UserWithThisGoogleAccountNotFound.ToString());

                return RedirectToAction("AuthProblem");
            }

            await Authenticate(cmsUserForAuthorize, false);

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult AuthProblem()
        {

            InnerAuthExceptionType problemType = InnerAuthExceptionType.UnknownProblem;

            if (string.IsNullOrEmpty(HttpContext.Session.GetString("ProblemType")) == false)
            {
                if (Enum.TryParse(HttpContext.Session.GetString("ProblemType"), out InnerAuthExceptionType problemType2))
                {
                    problemType = problemType2;
                }
            }

            string errMessage = string.Empty;

            switch (problemType)
            {
                case InnerAuthExceptionType.UnknownGoogleProblem:
                    {
                        errMessage = _localizer["UnknownProblem"];
                    }
                    break;
                case InnerAuthExceptionType.UserWithThisGoogleAccountNotFound:
                    {
                        errMessage = _localizer["UserWithThisGoogleAccountNotFound"];
                    }
                    break;
                case InnerAuthExceptionType.GoogleEmailNotVerified:
                    {
                        errMessage = _localizer["EmailNotVerified"];
                    }
                    break;
                case InnerAuthExceptionType.EmailNotConfirmed:
                    {
                        errMessage = _localizer["InnerEmailNotVerified"];
                    }
                    break;
                case InnerAuthExceptionType.UnknownProblem:
                    {
                        errMessage = _localizer["UnknownProblem"];
                    }
                    break;
            }

            return View((errMessage, problemType));
        }

        [HttpGet]
        public async Task<IActionResult> SignOut()
        {
            await HttpContext.SignOutAsync();

            return RedirectToAction("Index");
        }

        #region private methods

        private async Task Authenticate(CmsUser user, bool isRemember)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Id),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.CmsUserType.ToString())
            };

            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);

            if (isRemember)
            {
                await HttpContext.SignInAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(id),
                    new AuthenticationProperties
                    {
                        IsPersistent = true,
                        ExpiresUtc = DateTimeOffset.UtcNow.AddDays(30)
                    }
                );
            }
            else
            {
                await HttpContext.SignInAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id)
                );
            }
        }

        #endregion
    }
}