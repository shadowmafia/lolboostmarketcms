﻿namespace LoLBoostMarketCMS.Settings
{
    public class EmailSenderSettings
    {
        public string DisplayName { get; set; }
        public string DisplayEmail { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
