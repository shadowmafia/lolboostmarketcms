﻿namespace LoLBoostMarketCMS.Settings.ControllersSettings.UserProfileSettingsModels
{
    public class EditCmsUserSettings
    {
        public int MinUsernameLength { get; set; }
        public int MinPasswordLength { get; set; }
        public int ConfirmPasswordRandomHashLength { get; set; }
        public int VerifyEmailTimeOutInSeconds { get; set; }
        public int VerifyEmailTokenLifeTimeInMinutes { get; set; }

        public string EmailValidationRegex { get; set; }
    }
}