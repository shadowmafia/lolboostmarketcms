﻿using LoLBoostMarketCMS.Settings.ControllersSettings.UserProfileSettingsModels;

namespace LoLBoostMarketCMS.Settings.ControllersSettings
{
    public class UserProfileControllerSettings
    {
        public EditCmsUserSettings EditCmsUserSettings { get; set; }
    }
}
