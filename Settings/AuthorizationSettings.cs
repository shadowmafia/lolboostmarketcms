﻿using LoLBoostMarketCMS.Classes.Services.PasswordHashing;
using LoLBoostMarketCMS.Settings.AuthorizationSettingsModels;

namespace LoLBoostMarketCMS.Settings
{
    public class AuthorizationSettings
    {
        public GoogleSettings GoogleSettings { get; set; }

        public HashingOptions PaHashingOptions { get; set; }
    }
}
