﻿using LoLBoostMarketCMS.Settings.LocalizationSettingsModels;
using System.Collections.Generic;

namespace LoLBoostMarketCMS.Settings
{
    public class LocalizationSettings
    {
        public string DefaultLanguageCulture { get; set; }
        public List<LanguageItem> AllSupportedLanguages { get; set; }
    }
}
