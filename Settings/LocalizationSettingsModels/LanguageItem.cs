﻿namespace LoLBoostMarketCMS.Settings.LocalizationSettingsModels
{
    public class LanguageItem
    {
        public string LanguageName { get; set; }
        public string Culture { get; set; }
        public string FlagImgUrl { get; set; }
    }
}
