﻿using DataAccessLayerAbstraction.MarketData.Mapping;

namespace LoLBoostMarketCMS.Models.UserProfile
{
    public class CmsUserProfileModel
    {
        public CmsUser CmsUser { get; set; }
        public BoosterProfile BoosterProfile { get; set; }
    }
}
